from .Command import Command
from .Completers import tagCompleter

class BatchBibtex(Command):
    command = 'batch-bibtex'
    help = "Convert a list of dois into bibtex"

    def set_args(self, subparser):
        subparser.add_argument('file', metavar='TXTFILE', type=str)

    def run(self, args):
        from ..Database import Database
        from ..BaseWork import Work

        db = Database(dataDir=args.data_dir)

        for line in open(args.file):
            entry = Work.from_doi(line.strip())
            print(entry.bibtex)
